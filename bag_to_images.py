#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2016 Massachusetts Institute of Technology

"""Extract images from a rosbag.
"""
import os
import cv2
import rosbag
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import numpy as np
def main():
    """Extract a folder of images from a rosbag.
    """
    bag_file = '/home/hong/Desktop/zed_save_image/2018-08-22-13-37-19.bag' # bag file name
    output_dir = '/home/hong/Desktop/zed_save_image/uni_06/' # output directory
    image_topic = '/camera/left/image_raw/' # image topic
    bag = rosbag.Bag(bag_file, "r")
    bridge = CvBridge()
    count = 0
    cameraMatrix = np.array([[3.5847442850029023e+02, 0, 3.8840661559633401e+02], 
        [0, 3.5952665535350462e+02, 2.5476941553631312e+02], 
        [0, 0, 1]])
    distCoeffs = np.array([-3.0825216120347504e-01, 8.4251305214302186e-02, -1.5009319710179576e-04, 2.0170689406091280e-04])
    for topic, msg, t in bag.read_messages(topics=[image_topic]):
        cv_img = bridge.imgmsg_to_cv2(msg, desired_encoding="passthrough")
        print('The dimension of the origin image:')
        print(cv_img.shape)
        rotated_img = cv2.flip( cv_img, 0 ) # flip vertically
        rotated_img = cv2.flip( rotated_img, 1 ) # flip horizontally
        undist_img = cv2.undistort(rotated_img, cameraMatrix, distCoeffs) # undistort image
        stacked_img = cv2.merge((undist_img, undist_img, undist_img)) # stack single channel for two times to get rgb
        print('The dimension of the saved image:')
        print(stacked_img.shape)
        cv2.imwrite(os.path.join(output_dir, "%06i.png" % count), stacked_img)
        print "Wrote image %i" % count
        count += 1

    bag.close()

    return

if __name__ == '__main__':
    main()